<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");
  require_once("../classes/category.class.php");

  $category = new category;

  $result = $category->prepareQuery("SelectDB",["params" => $_GET]);
  $row = $result->fetch_assoc();

?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Edit Category</h1>
    <form action="storeCategory.php" method="POST">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="hidden" id="id" name="id" class="input-text" value="<?php echo $row['id']; ?>" />
        <input type="text" id="name" name="name" class="input-text" value="<?php echo $row['name']; ?>"/>
      </div>
      <div class="actions-form">
        <a href="categories.php" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
  <?php include_once ("footer.php"); ?>
 <!-- Footer --></body>
</html>
