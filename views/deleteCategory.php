<?php

    require_once("../classes/crud.class.php");
    require_once("../classes/category.class.php");

    $category = new category;

    $return = $category->prepareQuery("deleteDB", ["params" => $_GET]);

    if($return)
    {
        header('Location: categories.php?msg=sucesso');
    } else {
        header('Location: categories.php?msg=erro');
    }



