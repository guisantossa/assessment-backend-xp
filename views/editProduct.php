<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");
  require_once("../classes/product.class.php");
  require_once("../classes/category.class.php");

  $product = new Product;
  $category = new category;
  $product = $product->prepareQuery("selectDB", ["params" => $_GET]);
  $categories = $category->prepareQuery("selectDB");
  $row = $product->fetch_assoc();
?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form action="storeProduct.php?edit=1" method="POST">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name ="sku" class="input-text" value="<?php echo $row['SKU'];?>"/> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name='name' class="input-text" value="<?php echo $row['name'];?>"/> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" value="<?php echo number_format($row['price'], 2, ',', '.');?>"/> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" value="<?php echo $row['quantity'];?>"/> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple="multiple" id="category[]" class="input-text" name='category[]'>
        <?php while ($c = mysqli_fetch_assoc($categories)) { 
           $selected = $category->prepareQuery('SelectCount', ["table" => "product_categories","params" => array('product_id'=>$row["SKU"],'category_id'=>$c['id'])]);
           $select = $selected->fetch_assoc();
        ?>
          <option value="<?php echo $c['id']; ?>" <?php echo ($select['count']==1) ?  "selected=selected":""; ?>><?php echo $c['name']; ?></option>
        <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"><?php echo $row['description'];?></textarea>
      </div>
      <div class="actions-form">
        <a href="storeProduct.php" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->
  <script type="text/javascript">
        $(document).ready(function(){
        $('#price').mask('##0.00', {reverse: true});
        });
    </script>
  <!-- Footer -->
  <?php include_once ("footer.php"); ?>
 <!-- Footer --></body>
</html>
