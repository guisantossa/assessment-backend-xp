<?php
    require_once("../classes/crud.class.php");
    require_once("../classes/category.class.php");


    $category = new category;


    if(empty($_POST["id"]))
    {
        $return = $category->prepareQuery('insertDB', ["data" => $_POST]);
    } else {
        $return = $category->prepareQuery('updateDB', ["data" => $_POST, "params" => ["id" => $_POST["id"]]]); 
    }

    if($return)
    {
        header('Location: categories.php?msg=sucesso');
    } else {
        header('Location: categories.php?msg=erro');
    }

?>