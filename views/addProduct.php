<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");
  include_once("../classes/category.class.php");

  $category = new category;

  $result = $category->prepareQuery("selectDB");

?>

  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form action="storeProduct.php" method="POST" enctype='multipart/form-data'>
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name ="sku" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name='name' class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category[]" class="input-text" name='category[]'>
        <?php while ($row = mysqli_fetch_assoc($result)) {  ?>
          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"></textarea>
      </div>
      <div class="input-field">
        <label for="imagem" class="label">Image</label>
        <input type="file" id="image" name="image" class="input-text" /> 
      </div>
      <div class="actions-form">
        <a href="products.php" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->
  <script type="text/javascript">
        $(document).ready(function(){
        $('#price').mask('##0.00', {reverse: true});
        });
    </script>
  <!-- Footer -->
  <?php include_once ("footer.php"); ?>
 <!-- Footer --></body>
</html>
