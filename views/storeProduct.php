<?php
    require_once("../classes/crud.class.php");
    require_once("../classes/product.class.php");


    $product = new Product;
    if(empty($_GET["edit"]))
    {
        if($_FILES){
            $tmp = $_FILES[ 'image' ][ 'tmp_name' ];
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);
            $newName = uniqid(time()).'.'.$ext;
            $dest = "../images/product/".$newName;
            move_uploaded_file ( $tmp, $dest);
            $_POST["image"] = $dest;
        }
        $return = $product->prepareQuery('insertDB', ["data" => $_POST]);

        if($return){
            foreach($_POST['category'] as $key=>$value){
                $data['category_id'] = $value;
                $data['product_id'] = $_POST['sku'];
                $product->prepareQuery('insertDB', ["data" => $data, "table" => "product_categories"]);
            }
        }
    } else {
        $return = $product->prepareQuery('updateDB', array("data" => $_POST, "params" => ["sku" => $_POST['sku']]));
        if($return){
            $product->prepareQuery('deleteDB', ["table" => "product_categories","params" => ['product_id' => $_POST['sku']]]);
            foreach($_POST['category'] as $key=>$value){
                $data['category_id'] = $value;
                $data['product_id'] = $_POST['sku'];
                $product->prepareQuery('insertDB', ["data" => $data, "table" => "product_categories"]);
            }
        }
    }
    if($return)
    {
        header('Location: products.php?msg=Produto+Cadastrado+com+sucesso');
    } else {
        header('Location: products.php?msg=erro');
    }

?>