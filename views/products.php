<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");

  //Inclusão das classes a serem usadas
  include_once("../classes/product.class.php");
  include_once("../classes/category.class.php");

  $product = new Product;
  $result = $product->prepareQuery("selectDB");
  $category = new Category;
  $category->setTableJoin("product_categories");
  $category->setJoinCondition("product_categories.category_id = category.id");

  $mensagem = "";
  /* Mensgem de resposta a ser tratada
  
  if(!empty($_GET)){
    if($_GET["msg"]=="erro"){
      $mensagem = "Erro ao excluir produto!";
      $color = "red";
    } else {
      $mensagem = $_GET['msg'];
      $color = "green";
    }
  }
*/
?>

  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="addProduct.php" class="btn-action">Add new Product</a>
    </div>
    <div class="header-list-page">
      <h1 class="title" style="color:<?php echo $color; ?>"><?php echo $mensagem; ?></h1>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php while ($row = mysqli_fetch_assoc($result)) { ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $row['name'];?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $row['SKU'];?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">R$ <?php echo number_format($row['price'], 2, ',', '.');?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $row['quantity'];?></span>
        </td>
        <?php
          $categories = $category->prepareQuery("selectSimpleJoinDB", ["params" => array("product_id" => $row["SKU"])]);
        ?>
        <td class="data-grid-td">
           <span class="data-grid-cell-content">
            <?php
              while ($c = mysqli_fetch_assoc($categories)) {
                echo $c['name']."</br>";
            ?>
           <?php } ?>
           </span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <a href="editProduct.php?SKU=<?php echo $row['SKU']; ?>"><div class="action edit"><span>Edit</span></div></a>
            <a href="deleteProduct.php?SKU=<?php echo $row['SKU']; ?>"><div class="action delete"><span>Delete</span></div></a>
          </div>
        </td>
      </tr>
      <?php } ?>
    </table>
  </main>
  <!-- Main Content -->
  <script type="text/javascript">
  $(document).ready(function(){
    $(".delete").click( function(event) {
      var apagar = confirm('Deseja realmente excluir este registro?');
      if (apagar){

      }else{
          event.preventDefault();
      }	
    });
});
</script>
  <!-- Footer -->
  <?php include_once ("footer.php"); ?>
 <!-- Footer --></body>
</html>
