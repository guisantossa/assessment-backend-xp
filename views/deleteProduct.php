<?php

    require_once("../classes/crud.class.php");

    $product = new crud;
    $return = $product->prepareQuery("deleteDB", ["table" => "product_categories","params" => ["product_id" =>$_GET['SKU']]]);
    if($return)
        $return = $product->prepareQuery("deleteDB", ["table" => "product","params" => $_GET]);

    if($return)
    {
        header('Location: products.php?msg=sucesso');
    } else {
        header('Location: products.php?msg=erro');
    }


