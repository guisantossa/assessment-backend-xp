<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");
  require_once("../classes/category.class.php");

  $category = new category;

  $result = $category->prepareQuery("selectDB");

  $mensagem = "";

  /* mensagem de retorno a ser tratada
  if(!empty($_GET)){
    if($_GET["msg"]=="erro"){
      $mensagem = "Erro ao excluir produto!";
      $color = "red";
    } else {
      $mensagem = "Produto Excluido com Sucesso!";
      $color = "green";
    }
  }
  */
?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="addCategory.php" class="btn-action">Add new Category</a>
    </div>
    <div class="header-list-page">
      <h1 class="title" style="color:<?php echo $color; ?>"><?php echo $mensagem; ?></h1>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php while ($row = mysqli_fetch_assoc($result)) { ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $row['name'];?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $row['id'];?></span>
        </td>
        <td class="data-grid-td">
          <div class="actions">
            <a href="editCategory.php?id=<?php echo $row['id'];?>"><div class="action edit"><span>Edit</span></div></a>
            <a href="deleteCategory.php?id=<?php echo $row['id'];?>"><div class="action delete"><span>Delete</span></div></a>
          </div>
        </td>
      </tr>
      <?php } ?>
    </table>
  </main>
  <!-- Main Content -->
  <script type="text/javascript">
  $(document).ready(function(){
    $(".delete").click( function(event) {
      var apagar = confirm('Deseja realmente excluir este registro?');
      if (apagar){

      }else{
          event.preventDefault();
      }	
    });
});
</script>
  <!-- Footer -->
  <?php include_once ("footer.php"); ?>
 <!-- Footer --></body>
</html>
