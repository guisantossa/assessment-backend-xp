<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");
  include_once("../classes/product.class.php");

  $produtos = new Product;
  $result = $produtos->prepareQuery("selectDB");
  $produto = $produtos->prepareQuery("selectCount");
  $qnt = $produto->fetch_assoc();

?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      You have <?php echo $qnt['count']; ?> products added on this store: <a href="addProduct.php" class="btn-action">Add new Product</a>
    </div>
    <ul class="product-list">
    <?php 
      $i =0 ;
      while ($row = mysqli_fetch_assoc($result)) { 
      $i++; #ajudando na contagem para mostragem
      if($row['image']) # definindo se tem imagem no banco; Caso não tenha mostra uma imagem padrão
        $image = $row['image'];
      else
        $image = "../images/product/no-image.png";
    ?>
      <li>
        <div class="product-image">
          <img src="<?php echo $image; ?>" layout="responsive" width="164" height="145" alt="<?php echo $row['name']; ?>" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?php echo $row['name']; ?></span></div>
          <div class="product-price"><span class="special-price"><?php echo $row['quantity']; ?> available</span> <span>R$ <?php echo number_format($row['price'], 2, ',', '.'); ?></span></div>
        </div>
      </li>
      <?php
        if(($i%4)===0){
      ?>
    </ul>
    <ul class="product-list">
        <?php
        }
        ?>
    <?php } ?>
    </ul>
  </main>
  <!-- Main Content -->
<?php
  include_once("footer.php");
?>

