<?php
  // Inclusão do cabeçalho das páginas
  include_once("main.php");

  

?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    <form action="storeCategory.php" method="POST">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="name" name="name" class="input-text" />
      </div>
      <div class="actions-form">
        <a href="categories.php" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<?php include_once ("footer.php"); ?>
 <!-- Footer --></body>
</html>
