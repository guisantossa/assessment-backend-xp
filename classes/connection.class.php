<?php
    include_once('../config.php');
    /**
     * Classe que faz conexão com o bando de dados
     * Dados abaixo a serem mudados de acordo com a configuração do banco
     */
abstract class Connection{

    private $host = HOST;
    private $user = DBUSER;
    private $password = PASS;
    private $db = DATABASE;

    #Realizará a conexão com o banco de dados
    protected function conectaDB()
    {
        try{
            $Con = new mysqli($this->host,$this->user,$this->password,$this->db);
            return $Con;
        }catch (Exception $Erro){
            return $Erro->getMessage();
        }
    }
}
 
?> 