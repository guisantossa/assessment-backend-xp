<?php 



include_once("connection.class.php");

    /*
        Classe da CRUD
        Classe que faz toda interação com o banco de dados
        Select / Update / Insert/ Delete / Joins
    */

class Crud extends Connection
{

    #Atributos
    public $table;
    public $id;
    private $crud;
    private $query;
    private $data;
    private $params = "1=1";
    private $tableJoin;
    private $joinCondition;

   
    #zera as váriaveis após a execução/preparação da query
    private function destroy(){
        unset($this->crud);
        unset($this->data);
        unset($this->query);
        $this->params = "1=1";
    }

    /*
    * Preparação da query
    * Ele pega 2 parametros
    * type onde diz que execução será feita
    * $data onde passa os parametros a serem tratado, no momento data = dados a inserir, params = parametros das execuções 
    * e table caso seja necessários a utulização de tabelas auxiliares (algo a ser melhorado)
    */
    public function prepareQuery($type, $data = null)
    {
        #caso venha algum dado ele faz o tratamento
        if($data){
            $this->splitDataParams($data);
            if(is_array($this->params)){
                $this->setParams();
            }
        }
        $this->$type($this->table);
        $Con = $this->conectaDB();
        $this->Crud = $Con->query($this->query);
        $this->destroy();
        // Caso operação não de certo retorna um erro
        if($this->Crud)
        {
            return $this->Crud;
        } else {
            return false;
        }
    }

    #Método de Inserção
    private function insertDB(){
        foreach($this->data as $field => $value){
            if(!(is_array($value))) {
                $fields[] = $field;
                $values[] = $value; 
            }
        }
        $this->query = "INSERT INTO $this->table (".implode(",", $fields).") VALUES ('".implode("','",$values)."')";
    }

    #metodo update
    private function updateDB(){
        foreach($this->data as $key => $value) {
            $values[] = !is_array($value) ?" $key = '$value' ":"";
        }
        $values = array_filter($values);
        $values = implode(",", $values);
        $this->query = "UPDATE $this->table SET $values WHERE $this->params";
    }

    #Metodo de Delete
    private function deleteDB(){

        $this->query = "delete from $this->table WHERE $this->params";
    }

    #metodo de select
    private function selectDB(){
        $this->query = "Select * from $this->table WHERE $this->params";
    }

    #metodo de Join (a ser aprimorado)
    private function selectSimpleJoinDB(){
        $this->query = "Select * from $this->table INNER JOIN $this->tableJoin ON($this->joinCondition) WHERE $this->params";
    }

    #metodo de contagem
    private function selectCount(){
        $this->query = "Select count(*) as count from $this->table WHERE $this->params";
    }


    #tratando os dados recebidos
    private function splitDataParams($data){
        foreach ($data as $key=>$value){
            switch ($key){
                case "params":
                    $this->params = $value;
                    break;
                case "data":
                    $this->data = $value;
                    break;
                case "table":
                        $this->table = $value;
                        break;
                default:
                    $this->data = $value;
                    break;
            }
        }
    }

    #tratando os parametros
    private function setParams(){
        foreach($this->params as $key => $value) {
            $params[] = !is_array($value) ?" $key = '$value' ":null;
        }
        $this->params = implode(" AND ", $params);
    }

    #selecionando tabela para join (algo a ser melhorado)
    public function setTableJoin($table) {
        $this->tableJoin = $table;
    }

    #selecionando a condição para join (algo a ser melhorado)
    public function setJoinCondition($condition) {
        $this->joinCondition = $condition;
    }

}

?>